FROM node:20

RUN mkdir /workdir
WORKDIR /workdir
ADD package*.json /workdir/
RUN npm ci && ln -s /workdir/node_modules/netlify-cli/bin/run.js /usr/bin/netlify
